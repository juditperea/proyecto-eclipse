package excepcions;

public class StockInsuficientException extends Exception{
	//serial
	private static final long serialVersionUID = 1L;
	//Constructor por defecto
	public StockInsuficientException() {
		super();
	}

	//Constructor con el paràmetro message
	public StockInsuficientException(String message) {
		super(message);
	}


}
