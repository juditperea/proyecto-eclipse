package controlador;

import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import model.Client;
import model.Persistable;

public class ClientsDAO implements Persistable<Client>{//implementa la interfaz Persistable
	private Map<Integer, Client> clients = new HashMap<Integer, Client>();
	

	public Client guardar(Client cl) {
		
			return clients.put(cl.getIdPersona(), cl);//guarda el cliente y lo devuelve
		}

	@Override
	public Client eliminar(int idPersona) {
		clients.get(idPersona);//busca el cliente que tiene el id que le pasamos
		return clients.remove(idPersona);//lo elimina y devuelve el cliente que hemos eliminado
	}

	@Override
	public Client buscar(int idPersona) {//busca el cliente que tiene el id que le pasamos
		return clients.get(idPersona);//devuelve el cliente
	}

	@Override
	public TreeMap<Integer, Client> getMap() {//le pasamos a la funcion de getMap de Persistable el mapa de K Integer y V Client
		 if (clients == null) {//si el mapa está vacio
			    System.out.println("No hi ha dades per mostrar");
			    return null;//devuelve null
			  }
		return (TreeMap<Integer, Client>) clients;//si encuentra clientes, los devuelve
	}
}
