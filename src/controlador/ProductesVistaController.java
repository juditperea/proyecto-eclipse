package controlador;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Level;
import java.util.logging.Logger;

import excepcions.StockInsuficientException;
import model.ImportExportData;
import model.ImportExportJSON;
import model.ImportExportText;
import model.Pack;
import model.Producte;
import model.ProducteAbstract;
import model.ProductesDAO;

public class ProductesVistaController {

	// CONSTRUCTOR OBJETO VISTA CONTROLLER
	private ProductesDAO pd;
	private String nomFitxer = "productes";
	private String nomFitxer2 = "datos";
	private String nomFitxer3 = "productesJSON.dat";
	private ImportExportText importExportText = new ImportExportText();
	private ImportExportData importExportData = new ImportExportData();
	private ImportExportJSON importExportJSON = new ImportExportJSON();

	public ProductesVistaController(ProductesDAO pd) {// ya no hace falta pasar el dao a todas las funciones
		this.pd = pd;
	}

	// inici
	public void inici() throws StockInsuficientException, IOException {
		int idproducte = 0;
		int quantitat = 0;
		boolean seguir = true;
		int opcio;
		do {
			menu();
			opcio = UtilConsole.pedirInt();
			switch (opcio) {
			case 0:
				seguir = false;
				break;
			case 1:

				afegirProducte(10, false);// pasamos false a modificar
				break;
			case 2:
				System.out.println("Escriu el ID del producte que vols buscar: ");
				idproducte = UtilConsole.pedirInt();
				ProducteAbstract ptemp = pd.buscar(idproducte);// pedimos id del producto que queremos buscar
				if (ptemp != null) {// lo buscamos
					System.out.println(ptemp);// si lo encuentra, lo imprime

				} else {
					System.out.println("El producte no existeix.");
				}
				break;
			case 3:

				System.out.println("Escriu el ID del producte que vols modificar: ");
				idproducte = UtilConsole.pedirInt();// pedimos id del producto que queremos modificar
				ptemp = pd.buscar(idproducte);// buscamos el producto
				if (ptemp != null) {// si el producto existe
					afegirProducte(idproducte, true);// pasamos true a modificar, y el id para que no cambie el id sino
														// que modifique ese producto

				} else {
					System.out.println("El producte no existeix.");
				}
				break;
			case 4:
				eliminar();
				break;
			case 5:
				mostrar();
				break;
			case 6:
				System.out.println("Quin format vols importar?");
				System.out.println("1. Format text");
				System.out.println("2. Format data");
				System.out.println("3. Format JSON");
				opcio = UtilConsole.pedirInt();

				importar(opcio);

				break;
			case 7:
				System.out.println("Quin format vols exportar?");
				System.out.println("1. Format text");
				System.out.println("2. Format data");
				System.out.println("3. Format JSON");
				opcio = UtilConsole.pedirInt();

				exportar(opcio);

				break;
			case 8:
				System.out.println("Escriu el ID del producte al que vols afegir stock: ");
				idproducte = UtilConsole.pedirInt();
				System.out.println("Escriu la quantitat d'stock que vols afegir: ");
				quantitat = UtilConsole.pedirInt();
			afegirStock(idproducte,quantitat);
				break;
			case 9:
				System.out.println("Escriu el ID del producte al que vols treure stock: ");
				idproducte = UtilConsole.pedirInt();
			treureStock(idproducte);
				break;
			default:
				break;
			}
		} while (seguir);
	}

	/**
	 * 
	 * @param productedao
	 * @param idproducte
	 * @param modificar   SIRVE PARA DECIRLE SI QUEREMOS MODIFICAR O NO
	 */
	public void afegirProducte(int idproducte, boolean modificar) {

		ProducteAbstract prAbs = null;
		boolean crear = false;
		int opcio = 0;
		do {
			System.out.println("Que vols crear?");
			System.out.println("1. Producte");
			System.out.println("2. Pack");
			opcio = UtilConsole.pedirInt();
			if (opcio == 1 || opcio == 2) {
				crear = true;
			}

		} while (!crear);// solo creara algo si le damos un 1 o 2 como opcion
		if (!modificar) {// cuando elijamos la opcion de modificar, en esa parte del menu le diremos que
							// modificar=true
			// si modificar es false, es decir hemos llamado a la funcion desde la opcion
			// del menu de añadir, nos crea y añade un Producto/Pack
			do {
				System.out.println("Escriu l'ID:");// cuando creemos Packs tenemos que tener en cuenta que no tenga el
													// mismo
				// id que un Producto porque si tiene el mismo lo sobreescribiria
				idproducte = UtilConsole.pedirInt();
				if (pd.buscar(idproducte) != null) {// si el id ya esta en el hashmap
					System.out.println("Aquest ID ja ha sigut utilitzat.");// informamos de que ya existe ese id
				}
			} while (pd.buscar(idproducte) != null);// bucle que pide un id hasta que le damos uno que no esta repetido
		}
		System.out.println("Escriu el nom:");
		String nom = UtilConsole.pedirString();
		if (opcio == 1) {// si la opcion es 1, creamos producto
			System.out.println("Escriu el preu del producte:");
			double preuVenda = UtilConsole.pedirDouble();
			System.out.println("Escriu l'stock del producte:");
			int stock = UtilConsole.pedirInt();
			
			prAbs = new Producte(idproducte, nom, preuVenda, stock);// creamos nuevo Producto
		} else if (opcio == 2) {// si la opcion es 2, creamos pack
			int ids = -1;
			TreeSet<Producte> productes = new TreeSet<>();// va guardando los productos en un arraylist de productos
			System.out.println("Productes");
			System.out.println("Escriu el descompte: ");
			double percentatgeDescompte = UtilConsole.pedirDouble();
			prAbs = new Pack(idproducte, nom, productes, percentatgeDescompte);// creamos nuevo Pack
			do {
				System.out.println("Escriu un ID per afegir o -1 per sortir: ");
				ids = UtilConsole.pedirInt2();// pide ids de productos ya existentes
				// usamos un scanner que acepta numeros negativos para que no nos salte
				// un Index out of bounds exception
				ProducteAbstract p = pd.buscar(ids);
				if (p != null && p instanceof Producte) {// el bucle busca si es un producto y existe
					productes.add((Producte) p);// si existe lo añade al arraylist
					System.out.println("S'ha afegit el producte " + p.getIdproducte());
				}else if(ids != -1) {
					System.out.println("No s'ha pogut afegir el producte.");
				}
				
			} while (ids != -1);// sale del bucle cuando introducimos -1
			
		}
		if (prAbs != null) {// si el producto/pack existe
			pd.guardar(prAbs);// lo guarda
		} else {
			System.out.println("No s'ha pogut crear el producte/pack.");
		}

	}

	public void eliminar() {
		System.out.println("Quin producte vols esborrar? Escriu l'ID:");
		int idproducte = UtilConsole.pedirInt();
		pd.eliminar(idproducte);

	}

	public void mostrar() {
		IniciVistaController.imprimir(pd);
	}

	private void menu() {
		System.out.println("0. Sortir" + "\n" + "1. Introduir producte/pack" + "\n" + "2. Buscar producte" + "\n"
				+ "3. Modificar producte/pack" + "\n" + "4. Eliminar producte/pack" + "\n"
				+ "5. Mostrar tots els productes" + "\n" + "6. Importar productes/packs" + "\n"
				+ "7. Exportar productes/packs" + "\n" + "8. Afegir stock" + "\n" + "9. Treure stock" + "\n" + "Opcio:");

	}

	public void importar(int opcio) throws IOException {
		if (opcio == 1) {
			Map<Integer, ProducteAbstract> mapa = importExportText.importar(nomFitxer);
			if (mapa != null) {
				for (Entry<Integer, ProducteAbstract> clau : mapa.entrySet()) {
					pd.getMap().put(clau.getKey(), clau.getValue());
				}
				System.out.println(mapa);
			} else {
				System.out.println("No s'han pogut importar els productes.");
			}
		}
		if (opcio == 2) {
			Map<Integer, ProducteAbstract> mapa = importExportData.importar(nomFitxer2);
			if (mapa != null) {
				for (Entry<Integer, ProducteAbstract> clau : mapa.entrySet()) {
					pd.getMap().put(clau.getKey(), clau.getValue());
				}
				System.out.println(mapa);
			} else {
				System.out.println("No s'han pogut importar els productes.");
			}
		}
		if (opcio == 3) {
			Map<Integer, ProducteAbstract> mapa = importExportJSON.importar(nomFitxer3);
			if (mapa != null) {
				for (Entry<Integer, ProducteAbstract> clau : mapa.entrySet()) {
					pd.getMap().put(clau.getKey(), clau.getValue());
				}
				System.out.println(mapa);
			} else {
				System.out.println("No s'han pogut importar els productes.");
			}
		}
	}

	public void exportar(int opcio) {
		if (opcio == 1) {
			boolean resultado = importExportText.exportar(nomFitxer, pd);
			if (resultado) {
				System.out.println("Productes exportats correctament.");
			} else {
				System.out.println("No s'han pogut exportar els productes.");
			}
		}
		if (opcio == 2) {
			boolean resultado = importExportData.exportar(nomFitxer2, pd);
			if (resultado) {
				System.out.println("Productes exportats correctament.");
			} else {
				System.out.println("No s'han pogut exportar els productes.");
			}
		}
		if (opcio == 3) {
			boolean resultado = importExportJSON.exportar(nomFitxer3, pd);
			if (resultado) {
				System.out.println("Productes exportats correctament.");
			} else {
				System.out.println("No s'han pogut exportar els productes.");
			}
		}
	}

	public void afegirStock(int idproducte, int quantitat) {
		   Producte producte = (Producte) pd.buscar(idproducte);
		    if (producte == null) {
		        System.out.println("El ID del producte no es correcte.");
		        return;
		    }
		    producte.afegirStock(quantitat);
		}
	
	public void treureStock(int idproducte) {
		int quantitat = -1;
		Producte producte = (Producte) pd.buscar(idproducte);
do{
	    try {
	        
	        if (producte == null) {
	            System.out.println("El ID del producte no es correcte.");
	            return;
	        }
	    	System.out.println("Escriu la quantitat d'stock que vols treure: ");
	        quantitat = UtilConsole.pedirInt();
	        producte.treureStock(quantitat);
	    } catch (StockInsuficientException e) {
	    	Logger log = Logger.getLogger("logs");
	    	log.log(Level.WARNING, "La quantitat introduida es mes gran que el stock!", e);
	    }
}while(quantitat > 0 && quantitat >= producte.getStock());

	}
	
}
