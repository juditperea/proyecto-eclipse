package controlador;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import model.Persistable;
import model.Proveidor;

public class ProveidorsDAO implements Persistable<Proveidor>{//implementa la interficie Persistable
	private Map<Integer, Proveidor> proveidors = new HashMap<Integer, Proveidor>();

	@Override
	public Proveidor guardar(Proveidor prov) {
		proveidors.put(prov.getIdPersona(), prov);//guarda el proveedor y lo devuelve
		return prov;
	}

	@Override
	public Proveidor eliminar(int idPersona) {
		proveidors.get(idPersona);//busca el proveedor que tiene el id que le pasamos
		return proveidors.remove(idPersona);//lo elimina y devuelve el proveedor que hemos eliminado

	}

	@Override
	public Proveidor buscar(int idPersona) {//busca el proveedor que tiene el id que le pasamos
		return proveidors.get(idPersona);//devuelve el proveedor

	}

	@Override
	public TreeMap<Integer, Proveidor> getMap() {//le pasamos el mapa de Proveidor a ProveidorVistaController que le pasa a IniciVistaController
		if (proveidors == null) {//si el mapa esta vacio
			System.out.println("No hi ha dades per mostrar");//devuelve null
			return null;
		}
		return (TreeMap<Integer, Proveidor>) proveidors;//si no esta vacio, devuelve los proveedores
	}

	


	
}
