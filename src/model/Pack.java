package model;

import java.io.Serializable;
import java.util.Objects;
import java.util.TreeSet;

import controlador.UtilConsole;

public final class Pack extends ProducteAbstract implements Serializable{//hereda de ProducteAbstract
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	// variables
	private TreeSet<Producte> productes;
	double percentatgeDescompte;

	// constructors
	public Pack() {
		super();
	
	}

	public Pack(int idproducte, String nom,TreeSet<Producte> productes, double percentatgeDescompte) {
		super(idproducte, nom);
		this.productes = productes;
		this.percentatgeDescompte = percentatgeDescompte;
	}

	// getters i setters
	public TreeSet<Producte> getProductes() {
		return productes;
	}
	public double getPercentatgeDescompte() {
		return percentatgeDescompte;
	}

	public void setPercentatgeDescompte(double percentatgeDescompte) {
		this.percentatgeDescompte = percentatgeDescompte;
	}

	// metodes
	public void afegirProducte(ProducteAbstract prAbs) {
		productes.add((Producte) prAbs);
	}

	public void esborrarProducte() {
		System.out.println("Escriu l'id del producte que vols eliminar: ");
		int idproducte = UtilConsole.pedirInt();
		productes.remove(idproducte);
	}

	@Override
	public String toString() {
		String IdsProductes = "";
		for (ProducteAbstract producte : productes) {
			IdsProductes += producte.getIdproducte() + " | ";
		}
		return super.toString() + " Pack [ID's dels productes: " + IdsProductes + "]";
	}

	// override del metode equals
	@Override
	public boolean equals(Object obj) {
	    if (this == obj) {//si els objectes son identics
	        return true;
	    }
	    if (obj == null || getClass() != obj.getClass()) {//si es null o d'una classe diferent
	        return false;
	    }
	    Pack other = (Pack) obj;//cast de pack per comparar si els packs son iguals
	    return Objects.equals(productes, other.productes);//comparar directament les llistes de productes que conte,true o false
	}




	@Override
	public String toFileFormat() {
		  return getIdproducte() + "|" + getNom() + "|";
		
	}

	

}
