package model;

import java.util.HashMap;
import java.util.Map;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.reflect.TypeToken;

public class ImportExportJSON implements ImportExportable<ProducteAbstract>{

   
    @Override
    public Map<Integer, ProducteAbstract> importar(String nomFitxer) {
        Gson gson = new Gson(); //creem un objecte de la classe Gson per a convertir el contingut del fitxer a objectes Java
        Map<Integer, ProducteAbstract> productes = new HashMap<>(); //creem un mapa temporal de productes
        try {//llegim el contingut del fitxer i el convertim a un mapa de productes amb la classe TypeToken
            productes = gson.fromJson(new FileReader("res/" + nomFitxer), new TypeToken<Map<Integer, Producte>>() {}.getType());
        } catch (IOException e) {
            e.printStackTrace();//mostrem l'error en cas que hi hagi problemes en la lectura del fitxer
        }
        return productes; //retorna el mapa de productes
    }

    
    @Override
    public boolean exportar(String nomFitxer, Persistable<ProducteAbstract> dao) {
        
        Gson gson = new GsonBuilder().setPrettyPrinting().create();//creem un objecte Gson per convertir els objectes Java a JSON
        Map<Integer, ProducteAbstract> productes = dao.getMap();//obté el mapa que li passem amb el dao
        // Crea un objeto JsonObject para almacenar los objetos convertidos a JSON.
        JsonObject jsonObject = new JsonObject();//creem un objecte JsonObject per guardar els objectes convertits a JSON
  
        for (Map.Entry<Integer, ProducteAbstract> entry : productes.entrySet()) {//recorre el mapa i afegeix al objecte JsonObject
            jsonObject.add(entry.getKey().toString(), gson.toJsonTree(entry.getValue()));
        }
        try (FileWriter file = new FileWriter("res/" + nomFitxer)) {//creem un arxiu amb la ruta especificada i rep el contingut 
            gson.toJson(jsonObject, file);
           
            return true; //retorna true si s'ha exportat correctament
        } catch (IOException e) {//si hi ha un error ens mostra el missatge
            e.printStackTrace();
            return false;
        }
    }





}  
