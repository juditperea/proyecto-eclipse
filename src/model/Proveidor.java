package model;

import java.time.LocalDate;
import java.util.LinkedHashSet;
public class Proveidor extends PersonaAbstract{
	
//variables
	double dtoProntoPago;
	
//constructores
	public Proveidor(double dtoProntoPago) {
		super();
		this.dtoProntoPago = dtoProntoPago;
	}

	public Proveidor(int idpersona, String dNI, String nom, String cognoms, LocalDate dataNaixement, String email,
			LinkedHashSet<String>  telefons, Adreca adreca, double dtoProntoPago) {
		super(idpersona, dNI, nom, cognoms, dataNaixement, email, telefons, adreca);
		this.dtoProntoPago = dtoProntoPago;
	}

//getters i setters

	public double getDtoProntoPago() {
		return dtoProntoPago;
	}

	public void setDtoProntoPago(double dtoProntoPago) {
		this.dtoProntoPago = dtoProntoPago;
	}
	


//toString
	
	@Override
	public String toString() {
		return "Proveidor [=" + dtoProntoPago + ", Descompte =" + getDtoProntoPago()
				+ ", Id=" + getIdPersona() + ", DNI=" + getDNI() + ", Nom=" + getNom()
				+ ", Cognoms=" + getCognoms() + ", Data de naixement=" + getDataNaixement() + ", Email="
				+ getEmail() + ", Telefon=" + getTelefon() + ", Direccio=" + getAdreca() + ", Edat="
				+ getEdad() + "]";
	}


}
