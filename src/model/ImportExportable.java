package model;
import java.io.IOException;
import java.util.Map;


public interface ImportExportable<T> {
//metodes
	public Map<Integer, T> importar(String nomFitxer) throws IOException; 
	
	public boolean exportar(String nomFitxer, Persistable<T> dao);

	
	
	
	
	
}
