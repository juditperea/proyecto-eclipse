package model;

import java.io.Serializable;

import excepcions.StockInsuficientException;

public final class Producte extends ProducteAbstract implements Serializable{//hereda de ProducteAbstract
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	//variables
	private int stock;
	private double preuVenda;
	//constructors
	public Producte() {
		super();
	}
	public Producte(int idproducte, String nom, double preuVenda, int stock) {
		super(idproducte, nom);
		this.stock = stock;
		this.preuVenda = preuVenda;
	}
	//getters i setters
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public double getPreuVenda() {
		return preuVenda;
	}
	public void setPreuVenda(double preuVenda) {
		this.preuVenda = preuVenda;
	}
	//toString
	public String toString() {
		return super.toString() + " Producte [Stock =" + stock + ", Preu =" + preuVenda  + "]";
	}
	// override del mètode equals
	@Override
	public boolean equals(Object obj) {
		if (this == obj)// en el cas de que siguin iguals els objectes retorna true
			return true;
		if (obj == null)// si el objecte no existeix retorna false
			return false;
		ProducteAbstract other = (ProducteAbstract) obj;
		if (this.getNom().equals(other.getNom()))// si el nom es igual al nom de l'objecte que li donem
			return true;// retorna true
		return false;// si no es cap de les anteriors retorna false
	}
	@Override
	public String toFileFormat() {
		return getIdproducte() + "|" + getNom() + "|" + getPreuVenda() + "|" + getStock();
	}
	public void afegirStock(int quantitat){
          stock += quantitat;
	}
	public void treureStock(int quantitat) throws StockInsuficientException{
		  if (quantitat > stock) {
		        throw new StockInsuficientException("No hi ha suficient stock per restar l'stock desitjat.");
		    }
		    stock -= quantitat;
	}


}
