package model;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.Map;

public class ImportExportData implements ImportExportable<ProducteAbstract> {

	@Override
	public Map<Integer, ProducteAbstract> importar(String nomFitxer) {// nos devolvera un hashmap de producto,le pasamos
																		// el nombre del archivo
		Map<Integer, ProducteAbstract> productes = new HashMap<>();// creamos un mapa temporal de productos
		try (DataInputStream dis = new DataInputStream((new FileInputStream("res/" + nomFitxer + ".dat")))) {
			while (dis.available() > 0) { // leer todos los productos del archivo
				int IdProducte = dis.readInt();//leemos separando los datos: primero el id que es un int
				String Nom = dis.readUTF();//leemos el nombre que es string
				System.out.println(Nom);

				double PreuVenda = dis.readDouble();//leemos el precio que es un double
				int Stock = dis.readInt();//leemos stock que es un int
				Producte prod = new Producte(IdProducte, Nom, PreuVenda, Stock);//creamos un objeto de tipo producto
				productes.put(prod.getIdproducte(), prod);//añadimos el producto al hashmap de productos
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());// si hay algun error, salta el mensaje
		}
		return productes;// devuelve el mapa de productos
	}

	@Override
	public boolean exportar(String nomFitxer, Persistable<ProducteAbstract> dao) {
		try (DataOutputStream dos = new DataOutputStream((new FileOutputStream("res/" + nomFitxer + ".dat")))) {
			// creamos el objeto DataOutPutStream y FileOutputStream y le pasamos el nombre
			// del fichero que queremos escribir
			for (ProducteAbstract producto : dao.getMap().values()) {// iteramos en el mapa que le estamos pasando con
				System.out.println(producto);										// el dao
				if (producto instanceof Producte) {// si producto es un Producto(y no un Pack)

					String productoString = producto.toFileFormat() +//sirve para que se escriba en el formato deseado, separando por |
					System.lineSeparator();//para que cada producto se escriba en una linea
					dos.writeInt(producto.getIdproducte());//escribimos separando los datos: primero el id que es un int
					dos.writeUTF(producto.getNom());//escribimos el nombre que es string
					dos.writeDouble(((Producte) producto).getPreuVenda());//escribimos el precio que es un double,casteamos porque los packs no tienen precio
					dos.writeInt(((Producte) producto).getStock());//escribimos stock que es un int,casteamos porque los packs no tienen stock
				}
			}

			return true;// devuelve true si se ha exportado correctamente
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;// devuelve false y el nombre del error si algo falla
		}
	}

}
