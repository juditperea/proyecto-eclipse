package model;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import controlador.UtilConsole;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class ImportExportText implements ImportExportable<ProducteAbstract> {

	@Override
	public Map<Integer, ProducteAbstract> importar(String nomFitxer) throws IOException{
		Logger log = Logger.getLogger("logs");
		FileHandler fh = new FileHandler("res/log.txt", true);
		fh.setFormatter(new SimpleFormatter());
		log.addHandler(fh);
		log.setLevel(Level.ALL);
		log.info("Inicio");
		Map<Integer, ProducteAbstract> productes = new HashMap<>();
		Producte prod;
		Path Ruta = Paths.get("res/" + nomFitxer + ".txt");/*el metodo recibe como parámetro un objeto de la clase Path
        con la ruta que queremos comprobar*/
		if (Files.exists(Ruta)) {//comprueba que la ruta que pasamos existe, y si existe:
			//creamos un objeto de tipo File(archivo) y le pasamos como parámetro
			File file = new File("res/" + nomFitxer + ".txt");
			System.out.println("Vols veure el contingut del directori actual?(Si/No)");
			String directori = UtilConsole.pedirString();
			if (directori.equalsIgnoreCase("Si")) {
				archivosCarpeta("C:\\1DAW\\M03 PROGRAMACION\\UF3");//llamamos a la funcion y le pasamos la ruta para que liste sus archivos
			}
			try(Scanner sc = new Scanner(new BufferedReader(new FileReader(file)))) {
				//el nombre de la carpeta de la ruta(res) + variable nombreArchivo que le pasaremos desde el Vista Controller de Productos 
				//diciendole el nombre que tendra el archivo + ".txt" ya que será un archivo .txt
				while (sc.hasNextLine()) {//mientras haya mas lineas
					String[] split = sc.nextLine().split("\\|");//lee la siguiente y separa el contenido por | y lo mete en un array de strings 
					Integer id = Integer.valueOf(split[0]);//el id lo parsea ya que era un string y lo mete en la primera posicion del array
					String nombre = split[1];//lo mete en la segunda posicion, no tiene que parsear porque es string
					Double preuvenda = Double.valueOf(split[2]);//lo mete en la tercera posicion y parsea
					Integer stock = Integer.valueOf(split[3]);//lo mete en la cuarta posicion y parsea
					//creamos nuevo objeto de tipo producto
					prod = new Producte(id, nombre, preuvenda, stock);
					//cogemos con un get el id de producto, guardamos el producto con su id en el hashmap
					productes.put(prod.getIdproducte(), prod);
				}sc.close();//cerramos scanner
			}catch (IOException io) {
				log.log(Level.SEVERE,"Error al manipular el archivo: " + io.getMessage());
			} catch (Exception e) {
				System.out.println(e.getMessage());//si algo falla salta el error
			}
		}return productes;// devuelve el hashmap

	}

	// CON BUFFERED READER
	/*
	 * @Override public Map<Integer, ProducteAbstract> importar(String nomFitxer) {
	 * Map<Integer, ProducteAbstract> productes = new HashMap<Integer,
	 * ProducteAbstract>(); Producte prod; try(BufferedReader bfr = new
	 * BufferedReader(new FileReader("res/" + nomFitxer + ".txt"))) { //creamos un
	 * objeto de tipo BufferedReader y FileReader y le pasamos como parámetro el
	 * nombre del archivo String linea = ""; //NumberFormat nformat =
	 * NumberFormat.getInstance(Locale.ITALIAN);//sirve para que el double acepte la
	 * coma y no el punto while ((linea = bfr.readLine()) != null) {//si la linea no
	 * esta vacia String[] split = linea.split("\\|");//lee la siguiente y separa el
	 * contenido por | y lo mete en un array de strings Integer id =
	 * Integer.valueOf(split[0]);//el id lo parsea ya que era un string y lo mete en
	 * la primera posicion del array String nombre = split[1];//lo mete en la
	 * segunda posicion, no tiene que parsear porque es string Double preuvenda =
	 * Double.valueOf(split[2]);//lo mete en la tercera posicion y le da el formato
	 * local con su valor Integer stock = Integer.valueOf(split[3]);//lo mete en la
	 * cuarta posicion y parsea prod = new Producte(id, nombre, preuvenda,
	 * stock);//creamos nuevo objeto de tipo producto
	 * productes.put(prod.getIdproducte(), prod);//sacamos con un get el id de
	 * producto, guardamos el producto con su id en el hashmap }
	 * 
	 * } catch (Exception e) { System.out.println(e.getMessage());//si algo falla
	 * salta el error } return productes;//devuelve el hashmap }
	 */
	@Override
	public boolean exportar(String nomFitxer, Persistable<ProducteAbstract> dao) {
		try (BufferedWriter bw = new BufferedWriter(new FileWriter("res/" + nomFitxer + ".txt"))) {
			// creamos un objeto de tipo File(archivo) y le pasamos como parámetro el nombre
			// del archivo
			for (ProducteAbstract producto : dao.getMap().values()) {// iteramos en el mapa que le estamos pasando con
				// el dao
				// sacamos el valor de cada producto que hay en el mapa
				bw.write(producto.toFileFormat());// escribimos con el bw en el archivo todos los productos con el
				// formato del archivo
				// con la función que creamos en ProducteAbstract,Productos y Packs
				bw.newLine();// salto de linea
			}
			return true;// devuelve true si se ha hecho la exportación
		} catch (Exception e) {
			System.out.println("No esta funcionando correctamente.");
			return false;// devuelve false y nos informa con un mensaje si no ha funcionado bien
		}
	}

	// declaramos la funcion y le pasamos como parametro una ruta
	public static void archivosCarpeta(String ruta) {
		System.out.println("El contingut de la ruta especificada es: ");
		// la variable carpeta de tipo Path guarda la ruta
		Path carpeta = Paths.get(ruta);
		// se intenta guardar en la secuencia stream la secuencia de archivos
		try (var stream = Files.list(carpeta)) {
			// hacemos un for each que recorre la secuencia e imprime carpetas y archivos
			stream.forEach(System.out::println);
		} catch (IOException e) {// muestra las excepciones
			e.printStackTrace();
		}
	}
}
