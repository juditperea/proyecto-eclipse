package model;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.LinkedHashSet;

public abstract class PersonaAbstract implements Comparable<PersonaAbstract>{

//variables
	private static int NumeroPersones;
	private int idPersona;
	private String nom;
	private String cognoms;
	private String DNI;
	private LocalDate dataNaixement;
	private String email;
	private LinkedHashSet<String>  telefons;
	private Adreca adreca;
	
	//constructors
	
		public PersonaAbstract() {
			NumeroPersones++;
		}
		
		public PersonaAbstract(String dNI, String nom, String cognoms, int idpersona) {
			DNI = dNI;
			this.nom = nom;
			this.cognoms = cognoms;
			this.idPersona = idpersona;
			NumeroPersones++;
		}

		public PersonaAbstract(int idpersona, String dNI, String nom, String cognoms, LocalDate dataNaixement, String email, 
				LinkedHashSet<String>  telefons,
				Adreca adreca) {
			DNI = dNI;
			this.nom = nom;
			this.cognoms = cognoms;
			this.dataNaixement = dataNaixement;
			this.email = email;
			this.telefons = telefons;
			this.adreca = adreca;
			this.idPersona = idpersona;
			NumeroPersones++;
		}
		
	
	//getters i setters
	
	public static int getNumPersones() {
		return NumeroPersones;
	}
	public int getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}
	public String getDNI() {
		return DNI;
	}
	public void setDNI(String dNI) {
		DNI = dNI;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getCognoms() {
		return cognoms;
	}
	public void setCognoms(String cognoms) {
		this.cognoms = cognoms;
	}
	public LocalDate getDataNaixement() {
		return dataNaixement;
	}
	public void setDataNaixement(LocalDate dataNaixement) {
		this.dataNaixement = dataNaixement;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public LinkedHashSet<String>  getTelefon() {
		return telefons;
	}
	public void setTelefon(LinkedHashSet<String>  telefons) {
		this.telefons = telefons;
	}
	public Adreca getAdreca() {
		return adreca;
	}
	public void setAdreca(Adreca adreca) {
		this.adreca = adreca;
	}
	
	
	//funcions
	public int getEdad() {
		int edad=(int) ChronoUnit.YEARS.between(dataNaixement, LocalDate.now());
		return edad;
	}
	
	 public static int diferenciaEdad (PersonaAbstract p1, PersonaAbstract p2) {
		 int diferencia = (int) ChronoUnit.YEARS.between(p1.getDataNaixement(), p2.getDataNaixement());
		 diferencia = Math.abs(diferencia);
		return diferencia;
		
		 
	 }
	 
	@Override
	public String toString() {
		return "Persona [DNI = " + DNI + ", Nom = " + nom + ", Cognoms = " + cognoms + ", Data de naixement = " + dataNaixement
				+ ", Email = " + email + ", Telefon = " + telefons + ", Adreca = " + adreca + ", idpersona = " + idPersona + "]";
	}
	 public int compareTo(PersonaAbstract other) {//compara un altre id que li passem
	        return Integer.compare(this.idPersona, other.idPersona);//amb el id actual, retorna un enter
	        //que serà negatiu si l'id actual és menor al que s'està comparant, positiu si és major i 
	        //0 si son iguals
	    }
	 
}
